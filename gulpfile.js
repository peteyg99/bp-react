'use strict'

var gulp = require('gulp'),
    del = require('del'),
    rename = require('gulp-rename'),
    less = require('gulp-less'),
    react = require('gulp-react'),
    browserify = require('browserify'),
    babelify = require('babelify'),
    source = require('vinyl-source-stream')
 

var config = {
    lessSrcFile: 'site.less',
    lessSrcPath: './assets/less/',
    lessOutPath: './public/css/',
    jsSrcFile: 'app.jsx',
    jsOutFilePrefix: 'react-',
    jsSrcPath: './assets/js/',
    jsOutPath: './public/js/',
}
config.lessSrcFilePath = config.lessSrcPath + config.lessSrcFile
config.cleanLessPath = config.lessOutPath + '*.*'
config.jsSrcFilePath = config.jsSrcPath + config.jsSrcFile
config.cleanJsPath = config.jsOutPath + '*.*'
process.env.NODE_ENV = 'production'


gulp.task('default', ['bundle:js', 'bundle:less'])

gulp.task('dev', ['set-dev', 'bundle:js', 'bundle:less'])

gulp.task('set-dev', function() {
    process.env.NODE_ENV = 'development'
    gulp.watch(config.lessSrcPath + '**/*.less', ['bundle:less'])
    gulp.watch(config.jsSrcPath + '**/*.js*', ['bundle:js'])
})

gulp.task('bundle:less', function() {
    del(config.cleanLessPath)

    return gulp.src(config.lessSrcFilePath)
            .pipe(less())
            .pipe(gulp.dest(config.lessOutPath))
})

gulp.task('bundle:js', function() {
    del(config.cleanJsPath)

    return browserify(config.jsSrcFilePath, {
        extensions: ['.js', '.jsx'],
        debug: true
    })
    .transform(babelify, {
        presets: ['es2015', 'react'],
        plugins: ['transform-object-rest-spread']
    })
    .bundle()
    .on("error", function (err) { 
        console.log("Error : " + err.message) 
        if (!isProduction()) this.emit('end')
    })
    .pipe(source(config.jsSrcFile))
    .pipe(rename({ prefix: config.jsOutFilePrefix, extname: '.js' }))
    .pipe(gulp.dest(config.jsOutPath))
})

function isProduction(){
    return process.env.NODE_ENV === 'production'
}