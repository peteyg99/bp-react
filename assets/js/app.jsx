import React from 'react'
import ReactDOM from 'react-dom'

import HelloWorld from './components/HelloWorld'

(function() {
    "use strict"
    
    var App = React.createClass({
        render: function() {
            return (
                <div>
                    <h1>BoilerPlate - React</h1>
                    <HelloWorld message="world" />
                </div>
            )
        }
    })

    const appRoot = document.getElementById("root")

    ReactDOM.render(React.createElement(App), appRoot)
})()