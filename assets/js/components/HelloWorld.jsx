import React from 'react'

export default class HelloWorld extends React.Component {
    constructor(props) {
        super(props)
    }

    render(){
        return (
            <div className="hello-world">
                <p>Hello {this.props.message}</p>
            </div>
        )
    }
}